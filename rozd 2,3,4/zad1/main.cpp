#include "std_lib_facilities.h"

int main()
{
    int val1;
    int val2;
    cout << "Please enter int value 1\n";
    cin >> val1;
    cout << "Please enter int value 2\n";
    cin >> val2;
    if(val1 > val2)
        cout << "Value 1 is bigger\n";
    else if(val2 > val1)
        cout << "Value 2 is bigger\n";
    else if(val1 == val2)
        cout << "Values are equal\n";
    int sum = val1 +val2;
    cout << "sum is equal " << sum << '\n';
    int dif = abs(val1 - val2);
    cout << "difference between 2 values is " << dif << '\n';
    int pro = val1*val2;
    cout << "product of 2 values is " << pro << '\n';
    int rat = val1/val2;
    cout << "ratio of value 2 in value 1 is " << rat << '\n' << '\n';


    double val1_f;
    double val2_f;
    cout << "Please enter floating value 1\n";
    cin >> val1_f;
    cout << "Please enter floating value 2\n";
    cin >> val2_f;
    if(val1_f > val2_f)
        cout << "Value 1 is bigger\n";
    else if(val2_f > val1_f)
        cout << "Value 2 is bigger\n";
    else if(val1_f == val2_f)
        cout << "Values are equal\n";
    double sum_f = val1_f +val2_f;
    cout << "sum is equal " << sum_f << '\n';
    double dif_f = abs(val1_f - val2_f);
    cout << "difference between 2 values is " << dif_f << '\n';
    double pro_f = val1_f*val2_f;
    cout << "product of 2 values is " << pro_f << '\n';
    double rat_f = val1_f/val2_f;
    cout << "ratio of value 2 in value 1 is " << rat_f << '\n';

    // The outputs should not be the same especially when considering the product
}
