//Compile the �Goodbye, cruel world!� program and examine the error messages. Did the compiler find all the errors?\
 What did it suggest as the problems? Did the compiler get confused and diagnose more than four errors?\
Remove the errors one by one, starting with the lexically first, and see how\
the error messages change (and improve).

#include "std_lib_facilities.h"

int main()
{
 string s = "Goodbye, cruel world! ";
 cout << s << '\n';
}

//Try_This_4\main.cpp||In function 'int Main()':|\
Try_This_4\main.cpp|10|error: 'STRING' was not declared in this scope|\
Try_This_4\main.cpp|11|error: 'cOut' was not declared in this scope|\
Try_This_4\main.cpp|11|error: 'S' was not declared in this scope|\
Try_This_4\main.cpp|12|warning: no return statement in function returning non-void [-Wreturn-type]|

