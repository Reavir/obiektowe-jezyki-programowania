//Rewrite your currency converter program from the previous Try this to use a\
switch-statement. Add conversions from yuan and kroner. Which version of\
the program is easier to write, understand, and modify? Why?\

#include "std_lib_facilities.h"

int main()
{
 constexpr double dollars_per_yen = 111.074;
 constexpr double dollars_per_pound = 0.75337;
 constexpr double dollars_per_euro = 0.87769;
 constexpr double dollars_per_yuan = 6.68941;
 constexpr double dollars_per_kroner = 8.54934;
 double value = 1;
 char unit = ' ';
 cout<< "Please enter money followed by (e, y, p, u, k):\n";
 cin >> value >> unit;
 switch(unit){
 case 'e':
     cout << value << " yen == $" << value/dollars_per_euro << "\n";
     break;
 case 'y':
     cout << value << " yen == $" << value/dollars_per_yen << "\n";
     break;
 case 'p':
    cout << value << " pound == $" << value/dollars_per_pound << "\n";
    break;
 case 'u':
    cout << value << " yuan == $" << value/dollars_per_yuan << "\n";
    break;
 case 'k':
    cout << value << " korner == $" << value/dollars_per_kroner << "\n";
    break;
 }
}
//easier to write is version with if, because case can only be used with int and char
