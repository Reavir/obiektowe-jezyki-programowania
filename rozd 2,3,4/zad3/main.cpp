#include "std_lib_facilities.h"

int main()
{
    string unit;
    constexpr double cm_to_m = 0.01;
    constexpr double ft_to_m = 0.3048;
    constexpr double in_to_m = 0.0254;
    double largest, smallest, input, value, sum, times;
    vector<double> counts;
    largest = 0;
    smallest = 0;
    input = 0;
    cout << "enter value\n";
    while(cin >> value >> unit){
        if((unit != "cm") && (unit != "m") && (unit != "in") && (unit != "ft"))
            cout << "Unacceptable unit, try again\n";
        else
            if (unit == "cm")
                input = value * cm_to_m;
            else if (unit == "in")
                input = value * in_to_m;
            else if (unit == "ft")
                input = value * ft_to_m;
            else if (unit == "m")
                input = value;
            if (smallest == 0 && largest == 0) {
                    smallest = input;
                    largest = input;}
            if (input < smallest){
                cout << "smallest so far\n";
                smallest = input;}
            if (input > largest){
                cout << "largest so far\n";
                largest = input;}
            counts.push_back(input);
            sum += input;
            times += 1;
            cout << "enter another value\n";

    }
    sort(counts);
    cout << "results\n";
    cout << "vector of values\n";
    for (double i : counts)
        cout << i << "m  ";
    cout << "\n";
    cout << "The smallest value is: " << smallest << "m\n";
    cout << "The largest valur is: " << largest << "m\n";
    cout << "Number of values " << times  << "\n";
    cout << "The sum of the values is " << sum << "m\n";

}
