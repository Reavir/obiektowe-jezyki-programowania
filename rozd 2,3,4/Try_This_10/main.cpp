//Implement square() without using the multiplication operator; that is, do the\
x*x by repeated addition (start a variable result at 0 and add x to it x times).\
Then run some version of �the first program� using that square().

#include "std_lib_facilities.h"

int square(int x){
    int sum = 0;
    for (int i = 0; i < x; ++i){
        sum += x;
    }
    return sum;
}
// calculate and print a table of squares 0�99
int main()
{
    for (int i = 0; i<100; ++i)
    cout << i << '\t' << square(i) << '\n';
}
