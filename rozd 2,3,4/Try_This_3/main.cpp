// Get the �repeated word detection program� to run. Test it with the sentence\
She she laughed He He He because what he did did not look very very good\
good. How many repeated words were there? Why? What is the definition of\
word used here? What is the definition of repeated word? (For example, is She\
she a repetition?)\

#include <iostream>

using namespace std;

int main()
{
 string previous = " "; // previous word; initialized to �not a word�
 string current; // current word
 while (cin>>current) { // read a stream of words
    if (previous == current) // check if the word is the same as last
        cout << "repeated word: " << current << '\n';
 previous = current;
 }
}
//There are 4 repeated words, because\
It does matter if there is a capital letter in the word\
The definition of word used here is a string of characters w/o whitespace\
Repeated word is a string of exactly same characters

