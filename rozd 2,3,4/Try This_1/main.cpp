// Get the �name and age� example to run. Then, modify it to write out the age\
months: read the input in years and multiply (using the * operator) by 12.\
Read the age into a double to allow for children who can be very proud of\
Being five and a half years old rather than just five.

#include "hrad.h"

using namespace std;

// read name and age
int main()
{
 cout << "Please enter your first name and age\n";
 string first_name; // string variable
 double age; // integer variable
 cin >> first_name; // read a string
 cin >> age; // read an integer
 cout << "Hello, " << first_name << " "<< age<< " years old (which is " << age*12 << " months old)\n";

}
