//Use the example above as a model for a program that converts yen, euros,\
and pounds into dollars. If you like realism, you can find conversion rates on\
the web.

#include "std_lib_facilities.h"

int main()
{
 constexpr double dollars_per_yen = 111.074;
 constexpr double dollars_per_pound = 0.75337;
 constexpr double dollars_per_euro = 0.87769;
 double value = 1;
 char unit = ' ';
 cout<< "Please enter money followed by (e, y, p):\n";
 cin >> value >> unit;
 if (unit == 'e')
    cout << value << " euro == $" << value/dollars_per_euro << "\n";
 else if (unit == 'y')
    cout << value << " yen == $" << value/dollars_per_yen << "\n";
 else if (unit == 'p')
    cout << value << " pounds == $" << value/dollars_per_pound << "\n";
 else
    cout << "Sorry, I don't know a unit called '" << unit << "'\n";
}
