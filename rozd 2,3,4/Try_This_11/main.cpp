//Write a program that �bleeps� out words that you don�t like; that is, you\
read in words using cin and print them again on cout. If a word is among a\
few you have defined, you write out BLEEP instead of that word. Start with\
one �disliked word� such as\
string disliked = �Broccoli�;\
When that works, add a few more

#include "std_lib_facilities.h"

int main()
{
    vector<string> words;
    for(string word; cin>>word; ) // read whitespace-separated words
        words.push_back(word); // put into vector

    vector<string> not_like = {"Broccoli", "Carrot"};
    for(string word : words){
        int i = 0;
        for(string dislike : not_like){
            if (word == dislike)
                i = 1;
        }
        if (i == 1)
            cout << "BLEEP" << '\n';
        else
            cout << word << '\n';
    }
}

