#include "std_lib_facilities.h"
void game(int i)
{
    char choose;

    vector<string> ai = {"rock", "paper", "scissors"};
    while (true){
        cout << "Choose rock, paper or scissors(r, p, s): \n";
        cin >> choose;
        switch(choose){
        case 'r':
            cout << "Your choice is rock\n";
            if (ai[i] == "paper")
                cout << "You loose!\n";
            else if (ai[i] == "rock")
                cout << "Draw!\n";
            else if (ai[i] == "scissors")
                cout << "You win!\n";
            break;
        case 'p':
            cout << "Your choice is paper\n";
            if (ai[i] == "paper")
                cout << "Draw!\n";
            else if (ai[i] == "rock")
                cout << "You win!\n";
            else if (ai[i] == "scissors")
                cout << "You loose!\n";
            break;
        case 's':
            cout << "Your choice is scissors\n";
            if (ai[i] == "paper")
                cout << "You win!\n";
            else if (ai[i] == "rock")
                cout << "You loose!\n";
            else if (ai[i] == "scissors")
                cout << "Draw!\n";
            break;
        }
        i = (i + 1) % 3;
        game(i);
    }
}
int main()
{
    int i = 0;
    game(i);
}

