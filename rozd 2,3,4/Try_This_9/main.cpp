//Rewrite the character value example from the previous Try this to use a\
for-statement. Then modify your program to also write out a table of the\
integer values for uppercase letters and digits.

#include "std_lib_facilities.h"

int main()
{
    char ch = 'a';
    for (int i = 'a';i <= 'z'; ++i){ //lower case
        ch = i;
        cout << ch << '\t' << i << '\n';
    }
    for (int i = 'A';i <= 'Z'; ++i){ //upper case
        ch = i;
        cout << ch << '\t' << i << '\n';
    }
    for (int i = '0';i <= '9'; ++i){//digits
        ch = i;
        cout << ch << '\t' << i << '\n';
    }
}
