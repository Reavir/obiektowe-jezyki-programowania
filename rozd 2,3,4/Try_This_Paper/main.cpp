//Execute this program yourself using a piece of paper. Use the input The cat\
cat jumped. Even experienced programmers use this technique to visualize the\
actions of small sections of code that somehow don�t seem completely obvious.


#include "std_lib_facilities.h"

int main()
{
 string previous = " "; // previous word; initialized to �not a word�
 string current; // current word
 while (cin>>current) { // read a stream of words
        if (previous == current) // check if the word is the same as last
            cout << "repeated word: " << current << '\n';
 previous = current;
 }
}
// 1:
// While
//  cin >> current = The
// If
//   previous =/= current
// previous = current = The

// 2:
// While
//  cin >>current = cat
// If
//   previous =/= current
// previous = current = cat

// 3:
// While
//  cin >> cat
// If
//   previous == current
// cout << "repeated word: " << cat << '\n';
// previous = current = cat

// 4:
// While
//  cin >>current = jumped
// If
//   previous =/= current
// previous = current = jumped

//output: "repeated word: cat"
// end
